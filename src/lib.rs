pub mod parser;

use std::{collections::HashMap, fmt::Display, path::PathBuf};

use parser::{BashFunction, BashScript, BashVal};

/// Arch Linux package build description file.
/// Definitions taken from `man 5 PKGBUILD`.
#[allow(dead_code)]
#[derive(Debug, Default)]
pub struct PkgBuild {
    /// Filename corresponding to source
    script: Option<BashScript>,
    /// Either the name of the package or an array of names for split packages.
    /// Valid characters for members of this array are alphanumerics,
    /// and any of the following characters: “@ . _ + -”.
    /// Additionally, names are not allowed to start with hyphens or dots.
    pub pkgname: Vec<String>,
    /// The version of the software as released from the author (e.g., 2.7.1).
    /// The variable is not allowed to contain colons, forward slashes, hyphens
    /// or whitespace.
    pub pkgver: String,
    /// This is the release number specific to the distribution. This allows
    /// package maintainers to make updates to the package’s configure flags,
    /// for example. This is typically set to 1 for each new upstream software
    /// release and incremented for intermediate PKGBUILD updates. The variable
    /// is a positive integer, with an optional subrelease level specified by
    /// adding another positive integer separated by a period
    /// (i.e. in the form x.y).
    pub pkgrel: String,
    /// Used to force the package to be seen as newer than any previous
    /// versions with a lower epoch, even if the version number would normally
    /// not trigger such an upgrade. This value is required to be a positive
    /// integer; the default value if left unspecified is 0. This is useful
    /// when the version numbering scheme of a package changes (or is
    /// alphanumeric), breaking normal version comparison logic.
    pub epoch: Option<u64>,
    /// This should be a brief description of the package and its
    /// functionality. Try to keep the description to one line of text and to
    /// not use the package’s name.
    pub pkgdesc: Option<String>,
    /// This field contains a URL that is associated with the software being
    /// packaged. This is typically the project’s web site.
    pub url: Option<String>,
    /// This field specifies the license(s) that apply to the package. Commonly
    /// used licenses can be found in /usr/share/licenses/common. If you see
    /// the package’s license there, simply reference it in the license field
    /// (e.g., license=('GPL')). If the package provides a license not
    /// available in /usr/share/licenses/common, then you should include it in
    /// the package itself and set license=('custom') or
    /// license=('custom:LicenseName'). The license should be placed in
    /// $pkgdir/usr/share/licenses/$pkgname/ when building the package. If
    /// multiple licenses are applicable, list all of them:
    /// license=('GPL' 'FDL').
    pub license: Option<Vec<String>>,
    /// Specifies a special install script that is to be included in the
    /// package. This file should reside in the same directory as the PKGBUILD
    /// and will be copied into the package by makepkg. It does not need to be
    /// included in the source array (e.g., install=$pkgname.install).
    pub install: Option<String>,
    /// Specifies a changelog file that is to be included in the package. The
    /// changelog file should end in a single newline. This file should reside
    /// in the same directory as the PKGBUILD and will be copied into the
    /// package by makepkg. It does not need to be included in the source array
    /// (e.g., changelog=$pkgname.changelog).
    pub changelog: Option<String>,
    ///An array of source files required to build the package. Source files
    /// must either reside in the same directory as the PKGBUILD, or be a
    /// fully-qualified URL that makepkg can use to download the file. To
    /// simplify the maintenance of PKGBUILDs, use the $pkgname and $pkgver
    /// variables when specifying the download location, if possible.
    /// Compressed files will be extracted automatically unless found in the
    /// noextract array described below. Additional architecture-specific
    /// sources can be added by appending an underscore and the architecture
    /// name e.g., source_x86_64=(). There must be a corresponding integrity
    /// array with checksums, e.g.  cksums_x86_64=(). It is also possible to
    /// change the name of the downloaded file, which is helpful with weird
    /// URLs and for handling multiple source files with the same name. The
    /// syntax is: source=('filename::url'). makepkg also supports building
    /// developmental versions of packages using sources downloaded from
    /// version control systems (VCS). For more information, see Using VCS
    /// Sources below. Files in the source array with extensions .sig, .sign
    /// or, .asc are recognized by makepkg as PGP signatures and will be
    /// automatically used to verify the integrity of the corresponding source
    /// file.
    pub source: Option<Vec<String>>,
    pub source_arch: HashMap<String, Vec<String>>,
    /// An array of PGP fingerprints. If this array is non-empty, makepkg will
    /// only accept signatures from the keys listed here and will ignore the
    /// trust values from the keyring. If the source file was signed with a
    /// subkey, makepkg will still use the primary key for comparison. Only
    /// full fingerprints are accepted. They must be uppercase and must not
    /// contain whitespace characters.
    pub validpgpkeys: Option<Vec<String>>,
    /// An array of file names corresponding to those from the source array.
    /// Files listed here will not be extracted with the rest of the source
    /// files. This is useful for packages that use compressed data directly.
    pub noextract: Option<Vec<String>>,
    /// This array contains CRC checksums for every source file specified in
    /// the source array (in the same order). makepkg will use this to verify
    /// source file integrity during subsequent builds. If SKIP is put in the
    /// array in place of a normal hash, the integrity check for that source
    /// file will be skipped. To easily generate cksums, run
    /// “makepkg -g >> PKGBUILD”. If desired, move the cksums line to an
    /// appropriate location. Note that checksums generated by "makepkg -g"
    /// should be verified using checksum values provided by the software
    /// developer.
    pub cksums: Option<Vec<String>>,
    /// Alternative integrity checks that makepkg supports; these all behave
    /// similar to the cksums option described above. To enable use and
    /// generation of these checksums, be sure to set up the INTEGRITY_CHECK
    /// option in makepkg.conf(5).
    pub md5sums: Option<Vec<String>>,
    pub sha1sums: Option<Vec<String>>,
    pub sha224sums: Option<Vec<String>>,
    pub sha256sums: Option<Vec<String>>,
    pub sha384sums: Option<Vec<String>>,
    pub sha512sums: Option<Vec<String>>,
    pub b2sums: Option<Vec<String>>,
    /// An array of symbolic names that represent groups of packages, allowing
    /// you to install multiple packages by requesting a single target. For
    /// example, one could install all KDE packages by installing the kde
    /// group.
    pub groups: Option<Vec<String>>,
    /// Defines on which architectures the given package is available
    /// (e.g., arch=('i686' 'x86_64')). Packages that contain no architecture
    /// specific files should use arch=('any'). Valid characters for members of
    /// this array are alphanumerics and “_”.
    pub arch: Vec<String>,
    /// An array of file names, without preceding slashes, that should be
    /// backed up if the package is removed or upgraded. This is commonly used
    /// for packages placing configuration files in /etc. See
    /// "Handling Config Files" in pacman(8) for more information.
    pub backup: Option<Vec<String>>,
    /// An array of packages this package depends on to run. Entries in this
    /// list should be surrounded with single quotes and contain at least the
    /// package name. Entries can also include a version requirement of the
    /// form name<>version, where <> is one of five comparisons:
    /// >= (greater than or equal to), <= (less than or equal to),
    /// = (equal to), > (greater than), or < (less than). If the dependency
    /// name appears to be a library (ends with .so), makepkg will try to find
    /// a binary that depends on the library in the built package and append
    /// the version needed by the binary. Appending the version yourself
    /// disables automatic detection.
    pub depends: Option<Vec<String>>,
    /// Additional architecture-specific depends can be added by appending an
    /// underscore and the architecture name e.g., depends_x86_64=().
    pub depends_arch: HashMap<String, Vec<String>>,
    /// An array of packages this package depends on to build but are not
    /// needed at runtime. Packages in this list follow the same format as
    /// depends.
    pub makedepends: Option<Vec<String>>,
    /// Additional architecture-specific makedepends can be added by appending
    /// an underscore and the architecture name e.g., makedepends_x86_64=().
    pub makedepends_arch: HashMap<String, Vec<String>>,
    /// An array of packages this package depends on to run its test suite but
    /// are not needed at runtime. Packages in this list follow the same
    /// format as depends. These dependencies are only considered when the
    /// check() function is present and is to be run by makepkg.
    pub checkdepends: Option<Vec<String>>,
    /// Additional architecture-specific checkdepends can be added by appending
    /// an underscore and the architecture name e.g., checkdepends_x86_64=().
    pub checkdepends_arch: HashMap<String, Vec<String>>,
    /// An array of packages (and accompanying reasons) that are not essential
    /// for base functionality, but may be necessary to make full use of the
    /// contents of this package. optdepends are currently for informational
    /// purposes only and are not utilized by pacman during dependency
    /// resolution. Packages in this list follow the same format as depends,
    /// with an optional description appended. The format for specifying
    /// optdepends descriptions is:
    ///
    /// `optdepends=('python: for library bindings')`
    pub optdepends: Option<Vec<String>>,
    /// Additional architecture-specific optdepends can be added by appending
    /// an underscore and the architecture name e.g., optdepends_x86_64=().
    pub optdepends_arch: HashMap<String, Vec<String>>,
    /// An array of packages that will conflict with this package (i.e. they
    /// cannot both be installed at the same time). This directive follows the
    /// same format as depends. Versioned conflicts are supported using the
    /// operators as described in depends.
    pub conflicts: Option<Vec<String>>,
    /// Additional architecture-specific conflicts can be added by appending
    /// an underscore and the architecture name e.g., conflicts_x86_64=().
    pub conflicts_arch: HashMap<String, Vec<String>>,
    /// An array of “virtual provisions” this package provides. This allows a
    /// package to provide dependencies other than its own package name. For
    /// example, the dcron package can provide cron, which allows packages to
    /// depend on cron rather than dcron OR fcron. Versioned provisions are
    /// also possible, in the name=version format. For example, dcron can
    /// provide cron=2.0 to satisfy the cron>=2.0 dependency of other packages.
    /// Provisions involving the > and < operators are invalid as only specific
    /// versions of a package may be provided. If the provision name appears to
    /// be a library (ends with .so), makepkg will try to find the library in
    /// the built package and append the correct version. Appending the version
    /// yourself disables automatic detection.
    pub provides: Option<Vec<String>>,
    /// Additional architecture-specific provides can be added by appending an
    /// underscore and the architecture name e.g., provides_x86_64=().
    pub provides_arch: HashMap<String, Vec<String>>,
    /// An array of packages this package should replace. This can be used to
    /// handle renamed/combined packages. For example, if the j2re package is
    /// renamed to jre, this directive allows future upgrades to continue as
    /// expected even though the package has moved. Versioned replaces are
    /// supported using the operators as described in depends. Sysupgrade is
    /// currently the only pacman operation that utilizes this field. A normal
    /// sync or upgrade will not use its value.
    pub replaces: Option<Vec<String>>,
    /// Additional architecture-specific replaces can be added by appending an
    /// underscore and the architecture name e.g., replaces_x86_64=().
    pub replaces_arch: HashMap<String, Vec<String>>,
    /// This array allows you to override some of makepkg’s default behavior
    /// when building packages. To set an option, just include the option name
    /// in the options array. To reverse the default behavior, place an “!” at
    /// the front of the option. Only specify the options you specifically want
    /// to override, the rest will be taken from makepkg.conf(5).  NOTE: force
    /// is a now-removed option in favor of the top level epoch variable.
    ///
    /// `strip`:
    /// Strip symbols from binaries and libraries. If you frequently use a
    /// debugger on programs or libraries, it may be helpful to disable this
    /// option.
    ///
    /// `docs`:
    /// Save doc directories. If you wish to delete doc directories, specify
    /// !docs in the array.
    ///
    /// `libtool`:
    /// Leave libtool (.la) files in packages. Specify !libtool to remove them.
    ///
    /// `staticlibs`:
    /// Leave static library (.a) files in packages. Specify !staticlibs to
    /// remove them (if they have a shared counterpart).
    ///
    /// `emptydirs`:
    ///     Leave empty directories in packages.
    ///
    /// `zipman`:
    ///     Compress man and info pages with gzip.
    ///
    /// `ccache`:
    /// Allow the use of ccache during build(). More useful in its negative
    /// form !ccache with select packages that have problems building with
    /// ccache.
    ///
    /// `distcc`:
    /// Allow the use of distcc during build(). More useful in its negative
    /// form !distcc with select packages that have problems building with
    /// distcc.
    ///
    /// `buildflags`:
    /// Allow the use of user-specific buildflags
    /// (CPPFLAGS, CFLAGS, CXXFLAGS, LDFLAGS) during build() as specified in
    /// makepkg.conf(5). More useful in its negative form !buildflags with
    /// select packages that have problems building with custom buildflags.
    ///
    /// `makeflags`:
    /// Allow the use of user-specific makeflags during build() as specified
    /// in makepkg.conf(5). More useful in its negative form !makeflags with
    /// select packages that have problems building with custom makeflags such
    /// as -j2 (or higher).
    ///
    /// `debug`:
    /// Add the user-specified debug flags (DEBUG_CFLAGS, DEBUG_CXXFLAGS) to
    /// their counterpart buildflags as specified in makepkg.conf(5). When used
    /// in combination with the ‘strip’ option, a separate package containing
    /// the debug symbols is created.
    ///
    /// `lto`:
    /// Enable building packages using link time optimization. Adds -flto to
    /// both CFLAGS and CXXFLAGS.
    pub options: Option<Vec<String>>,

    /// The package() function is used to install files into the directory that
    /// will become the root directory of the built package and is run after
    /// all the optional functions listed below. The packaging stage is run
    /// using fakeroot to ensure correct file permissions in the resulting
    /// package. All other functions will be run as the user calling makepkg.
    pub package: String,
    /// An optional prepare() function can be specified in which operations to
    /// prepare the sources for building, such as patching, are performed. This
    /// function is run after the source extraction and before the build()
    /// function. The prepare() function is skipped when source extraction is
    /// skipped.
    pub prepare: Option<String>,
    /// The optional build() function is used to compile and/or adjust the
    /// source files in preparation to be installed by the package() function.
    pub build: Option<String>,
    /// An optional check() function can be specified in which a package’s
    /// test-suite may be run. This function is run between the build() and
    /// package() functions. Be sure any exotic commands used are covered by
    /// the checkdepends array.
    pub check: Option<String>,

    /// The name used to refer to the group of packages in the output of
    /// makepkg and in the naming of source-only tarballs. If not specified,
    /// the first element in the pkgname array is used. Valid characters for
    /// this variable are alphanumerics, and any of the following characters:
    /// “@ . _ + -”. Additionally, the variable is not allowed to start with
    /// hyphens or dots.
    pub pkgbase: Option<String>,

    /// Run right before files are extracted. One argument is passed: new
    /// package full version string.
    pub pre_install: Option<String>,
    /// Run right after files are extracted. One argument is passed: new
    /// package full version string.
    pub post_install: Option<String>,
    /// Run right before files are extracted. Two arguments are passed in this
    /// order: new package full version string, old package full version
    /// string.
    pub pre_upgrade: Option<String>,
    /// Run after files are extracted. Two arguments are passed in this order:
    /// new package full version string, old package full version string.
    pub post_upgrade: Option<String>,
    /// Run right before files are removed. One argument is passed: old package
    /// full version string.
    pub pre_remove: Option<String>,
    /// Run right after files are removed. One argument is passed: old package
    /// full version string.
    pub post_remove: Option<String>,
}

#[derive(Debug)]
pub enum AssignError {
    UnknownKey { key: String },
}

impl Display for AssignError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AssignError::UnknownKey { key } => f.write_fmt(format_args!("Unknown key: {}", key)),
        }
    }
}

pub type AssignResult = Result<(), AssignError>;

fn assign_var(pkb: &mut PkgBuild, key: &str, val: &BashVal) -> AssignResult {
    match val {
        BashVal::ValArray(val) => {
            let val: Vec<String> = val.into_iter().map(|e| e.string.to_owned()).collect();
            match key.split_once('_') {
                Some((key, arch)) => match key {
                    "source" => {
                        pkb.source_arch.insert(arch.to_string(), val);
                    }
                    "depends" => {
                        pkb.depends_arch.insert(arch.to_string(), val);
                    }
                    "makedepends" => {
                        pkb.makedepends_arch.insert(arch.to_string(), val);
                    }
                    "checkdepends" => {
                        pkb.checkdepends_arch.insert(arch.to_string(), val);
                    }
                    "optdepends" => {
                        pkb.optdepends_arch.insert(arch.to_string(), val);
                    }
                    "conflicts" => {
                        pkb.conflicts_arch.insert(arch.to_string(), val);
                    }
                    "provides" => {
                        pkb.provides_arch.insert(arch.to_string(), val);
                    }
                    "replaces" => {
                        pkb.replaces_arch.insert(arch.to_string(), val);
                    }
                    _ => {}
                },
                None => match key {
                    "pkgname" => pkb.pkgname = val,
                    "license" => pkb.license = Some(val),
                    "source" => pkb.source = Some(val),
                    "validpgpkeys" => pkb.validpgpkeys = Some(val),
                    "noextract" => pkb.noextract = Some(val),
                    "cksums" => pkb.cksums = Some(val),
                    "md5sums" => pkb.md5sums = Some(val),
                    "sha224sums" => pkb.sha224sums = Some(val),
                    "sha256sums" => pkb.sha256sums = Some(val),
                    "sha384sums" => pkb.sha384sums = Some(val),
                    "sha512sums" => pkb.sha512sums = Some(val),
                    "b2sums" => pkb.b2sums = Some(val),
                    "groups" => pkb.groups = Some(val),
                    "arch" => pkb.arch = val,
                    "backup" => pkb.backup = Some(val),
                    "depends" => pkb.depends = Some(val),
                    "makedepends" => pkb.makedepends = Some(val),
                    "checkdepends" => pkb.checkdepends = Some(val),
                    "optdepends" => pkb.optdepends = Some(val),
                    "conflicts" => pkb.conflicts = Some(val),
                    "provides" => pkb.provides = Some(val),
                    "replaces" => pkb.replaces = Some(val),
                    "options" => pkb.options = Some(val),
                    _ => {
                        return Err(AssignError::UnknownKey {
                            key: key.to_string(),
                        })
                    }
                },
            }
        }
        BashVal::ValString(val) => match key {
            "pkgname" => pkb.pkgname = vec![val.string.to_owned()],
            "pkgver" => pkb.pkgver = val.string.to_owned(),
            "pkgrel" => pkb.pkgrel = val.string.to_owned(),
            "epoch" => pkb.epoch = Some(u64::from_str_radix(&val.string, 10).unwrap()),
            "pkgdesc" => pkb.pkgdesc = Some(val.string.to_owned()),
            "url" => pkb.url = Some(val.string.to_owned()),
            "install" => pkb.install = Some(val.string.to_owned()),
            "changelog" => pkb.changelog = Some(val.string.to_owned()),
            "pkgbase" => pkb.pkgbase = Some(val.string.to_owned()),
            _ => {
                return Err(AssignError::UnknownKey {
                    key: key.to_string(),
                })
            }
        },
    }
    Ok(())
}

fn assign_fun(pkb: &mut PkgBuild, fun: &BashFunction) -> AssignResult {
    match fun.name.as_str() {
        "package" => pkb.package = fun.content.to_owned(),
        "prepare" => pkb.prepare = Some(fun.content.to_owned()),
        "build" => pkb.build = Some(fun.content.to_owned()),
        "check" => pkb.check = Some(fun.content.to_owned()),
        _ => {
            return Err(AssignError::UnknownKey {
                key: fun.name.clone(),
            })
        }
    }
    Ok(())
}

#[allow(unreachable_patterns)]
impl PkgBuild {
    /// Parse a PKGBUILD from a file at a given path.
    pub fn open(path: PathBuf) -> Result<Self, String> {
        // Parse file as bash
        let mut ret = PkgBuild::default();
        ret.script = Some(BashScript::open(path).unwrap());

        // Parse bash values
        for l in ret.script.as_ref().unwrap().content.clone().iter() {
            match l {
                parser::BashType::Comment(_com) => {}
                parser::BashType::Var(var) => match assign_var(&mut ret, &var.key, &var.value) {
                    Ok(_) => {}
                    Err(e) => match e {
                        // Ignore unknown keys
                        AssignError::UnknownKey { key: _ } => {}
                        _ => return Err(format!("Failed to parse {:?}: {}", var, e)),
                    },
                },
                parser::BashType::Function(fun) => match assign_fun(&mut ret, &fun) {
                    Ok(_) => {}
                    Err(e) => match e {
                        // Ignore unknown keys
                        AssignError::UnknownKey { key: _ } => {}
                        _ => return Err(format!("Failed to parse {:?}: {}", fun, e)),
                    },
                },
            }
        }
        Ok(ret)
    }

    /// Save, contents will be overwritten.
    pub fn save(&mut self) -> Result<(), String> {
        if let Some(s) = &mut self.script {
            // Update script with current values
            for var in s.content.iter_mut() {
                match var {
                    parser::BashType::Comment(_) => {}
                    // This is the wrong way to go about this
                    // We should parse all fields and set them accordingly
                    // This way, only existing fields are updated,
                    // and removed fields will cause panics
                    parser::BashType::Var(var) => {
                        var.value = match var.key.as_str() {
                            "pkgname" => self.pkgname.clone().into(),
                            "pkgver" => self.pkgver.clone().into(),
                            "pkgrel" => self.pkgrel.clone().into(),
                            "epoch" => self.epoch.clone().unwrap().to_string().into(),
                            "pkgdesc" => self.pkgdesc.clone().unwrap().into(),
                            "url" => self.url.clone().unwrap().into(),
                            "license" => self.license.clone().unwrap().into(),
                            "install" => self.install.clone().unwrap().into(),
                            "changelog" => self.changelog.clone().unwrap().into(),
                            "source" => self.source.clone().unwrap().into(),
                            "validpgpkeys" => self.validpgpkeys.clone().unwrap().into(),
                            "noextract" => self.noextract.clone().unwrap().into(),
                            "cksums" => self.cksums.clone().unwrap().into(),
                            "md5sums" => self.md5sums.clone().unwrap().into(),
                            "sha224sums" => self.sha224sums.clone().unwrap().into(),
                            "sha256sums" => self.sha256sums.clone().unwrap().into(),
                            "sha384sums" => self.sha384sums.clone().unwrap().into(),
                            "sha512sums" => self.sha512sums.clone().unwrap().into(),
                            "b2sums" => self.b2sums.clone().unwrap().into(),
                            "groups" => self.groups.clone().unwrap().into(),
                            "arch" => self.arch.clone().into(),
                            "backup" => self.backup.clone().unwrap().into(),
                            "depends" => self.depends.clone().unwrap().into(),
                            "makedepends" => self.makedepends.clone().unwrap().into(),
                            "checkdepends" => self.checkdepends.clone().unwrap().into(),
                            "optdepends" => self.optdepends.clone().unwrap().into(),
                            "conflicts" => self.conflicts.clone().unwrap().into(),
                            "provides" => self.provides.clone().unwrap().into(),
                            "replaces" => self.replaces.clone().unwrap().into(),
                            "options" => self.options.clone().unwrap().into(),
                            "pkgbase" => self.pkgbase.clone().unwrap().into(),
                            _ => {
                                continue;
                            }
                        }
                    }
                    parser::BashType::Function(fun) => {
                        fun.content = match fun.name.as_str() {
                            "package" => self.package.clone(),
                            "prepare" => self.prepare.clone().unwrap(),
                            "build" => self.build.clone().unwrap(),
                            "check" => self.check.clone().unwrap(),
                            _ => {
                                continue;
                            }
                        }
                    }
                }
            }
            return s.save();
        }
        Err("No script backing PKGBUILD; unable to save.".to_string())
    }
}

#[cfg(test)]

mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
