use std::{
    fs::File,
    io::{Read, Seek, Write},
    path::PathBuf,
};

use nom::{
    bytes::complete::{tag, take_till, take_while, take_while1},
    character::complete::{alphanumeric1, char, space0},
    multi::{many0, many1, separated_list0, separated_list1},
    sequence::{delimited, preceded, terminated, tuple},
    IResult,
};

/// Represents a bash comment
#[derive(Debug, Clone)]
pub(crate) struct BashComment {
    pub text: String,
}

impl BashComment {
    pub(crate) fn parse(s: &str) -> IResult<&str, Self> {
        let (s, (_, text)) = tuple((tag("#"), take_till(|c| c == '\n')))(s)?;
        Ok((
            s,
            Self {
                text: text.to_string(),
            },
        ))
    }
}

/// Represents a bash string, with optional comment
#[derive(Debug, Clone)]
pub(crate) struct BashString {
    pub string: String,
    pub comment: Option<BashComment>,
}

impl From<String> for BashString {
    fn from(string: String) -> Self {
        BashString {
            string,
            comment: None,
        }
    }
}

impl Into<String> for BashString {
    fn into(self) -> String {
        self.string
    }
}

impl BashString {
    pub(crate) fn parse(s: &str) -> IResult<&str, Self> {
        let sc = s.chars().nth(0).unwrap();
        let (s, (mut string, comment)) = match sc {
            '\'' | '"' => tuple((
                delimited(char(sc), take_till(|c| c == sc), char(sc)),
                many0(BashComment::parse),
            ))(s),
            _ => tuple((
                take_till(|c: char| c == '#' || c == ')' || c.is_whitespace()),
                many0(BashComment::parse),
            ))(s),
        }?;
        let comment = match comment.len() > 0 {
            true => Some(comment[0].clone()),
            false => None,
        };

        let mut _temp_s = String::new();
        if sc == '\'' || sc == '"' {
            _temp_s = format!("{}{}{}", sc, string, sc);
            string = _temp_s.as_str();
        }

        Ok((
            s,
            Self {
                string: string.to_string(),
                comment,
            },
        ))
    }
}

/// Represents possible values for a bash variable
#[derive(Debug, Clone)]
pub(crate) enum BashVal {
    ValString(BashString),
    ValArray(Vec<BashString>),
}

impl From<String> for BashVal {
    fn from(s: String) -> Self {
        BashVal::ValString(s.into())
    }
}

impl From<Vec<String>> for BashVal {
    fn from(vs: Vec<String>) -> Self {
        BashVal::ValArray(
            vs.into_iter()
                .map(|e| e.into())
                .collect::<Vec<BashString>>(),
        )
    }
}

impl BashVal {
    pub(crate) fn parse(s: &str) -> IResult<&str, Self> {
        match s.starts_with('(') {
            true => {
                let (s, v) = delimited(
                    char('('),
                    separated_list1(take_while1(char::is_whitespace), BashString::parse),
                    char(')'),
                )(s)?;
                Ok((s, BashVal::ValArray(v)))
            }
            false => {
                let (s, v) = BashString::parse(s)?;
                Ok((s, BashVal::ValString(v)))
            }
        }
    }
}

/// Represents a bash variable
#[derive(Debug, Clone)]
pub(crate) struct BashVar {
    pub key: String,
    pub value: BashVal,
}

impl BashVar {
    pub(crate) fn parse(s: &str) -> IResult<&str, Self> {
        let (s, (k, _, value)) = tuple((take_till(|c| c == '='), char('='), BashVal::parse))(s)?;
        Ok((
            s,
            Self {
                key: k.to_string(),
                value,
            },
        ))
    }
}

/// Represents a bash function
#[derive(Debug, Clone)]
pub(crate) struct BashFunction {
    pub name: String,
    pub args: Vec<String>,
    pub content: String,
}

/// Parses a block of text beginning with `{` and ending with `}`.
/// Handles nested blocks.
fn function_body(s: &str) -> IResult<&str, &str> {
    let mut levels = 1;
    let mut c = s.chars().enumerate();
    let mut i = 0;
    while let Some(c) = c.next() {
        match c.1 {
            '{' => levels += 1,
            '}' => levels -= 1,
            _ => {}
        }
        if levels <= 0 {
            i = c.0;
            break;
        }
    }
    let s = s.split_at(i);
    Ok((s.1, s.0))
}

impl BashFunction {
    pub(crate) fn parse(s: &str) -> IResult<&str, Self> {
        // This needs work, parsing nested blocks for example is broken
        let (s, (n, a, c)) = tuple((
            take_till(|c| c == '('),
            delimited(
                char('('),
                separated_list0(take_while1(char::is_whitespace), alphanumeric1),
                char(')'),
            ),
            preceded(space0, delimited(char('{'), function_body, char('}'))),
        ))(s)?;
        Ok((
            s,
            Self {
                name: n.to_string(),
                args: a.iter().map(|a| a.to_string()).collect(),
                content: c.to_string(),
            },
        ))
    }
}

/// Represents possible bash types
#[derive(Debug, Clone)]
pub(crate) enum BashType {
    Comment(BashComment),
    Var(BashVar),
    Function(BashFunction),
}

impl BashType {
    pub(crate) fn parse(s: &str) -> IResult<&str, Self> {
        let (mut s, t) = match s.starts_with('#') {
            true => {
                let (s, c) = BashComment::parse(s)?;
                (s, BashType::Comment(c))
            }
            false => match s.lines().nth(0) {
                Some(l) => match l.contains('=') {
                    true => {
                        let (s, v) = BashVar::parse(s)?;
                        (s, BashType::Var(v))
                    }
                    false => {
                        let (s, f) = BashFunction::parse(s)?;
                        (s, BashType::Function(f))
                    }
                },
                None => {
                    return Err(nom::Err::Error(nom::error::Error::new(
                        s,
                        nom::error::ErrorKind::Eof,
                    )))
                }
            },
        };
        s = s.trim_start_matches(char::is_whitespace);
        Ok((s, t))
    }
}

pub(crate) fn parse_bash(s: &str) -> IResult<&str, Vec<BashType>> {
    terminated(many1(BashType::parse), take_while(char::is_whitespace))(s)
}

#[derive(Debug)]
pub(crate) struct BashScript {
    pub content: Vec<BashType>,
    file: File,
    pub filename: PathBuf,
}

impl BashScript {
    /// Attempt ot open a file and parse it as a bash script.
    pub fn open(filename: PathBuf) -> Result<Self, ()> {
        let mut file = match File::options()
            .append(true)
            .read(true)
            .open(filename.clone())
        {
            Ok(f) => f,
            Err(_) => return Err(()),
        };
        let mut s = String::new();
        file.read_to_string(&mut s);
        let content = match parse_bash(&s) {
            Ok(c) => c.1,
            Err(_) => return Err(()),
        };
        Ok(Self {
            content,
            file,
            filename,
        })
    }

    /// Save contents to file.
    pub fn save(&mut self) -> Result<(), String> {
        self.file.set_len(0);
        self.file.seek(std::io::SeekFrom::Start(0));
        for i in &self.content {
            let text = match i {
                BashType::Comment(c) => {
                    format!("#{}\n", c.text)
                }
                BashType::Var(v) => match &v.value {
                    BashVal::ValString(s) => match &s.comment {
                        Some(c) => {
                            format!("{}={} #{}\n", v.key, s.string, c.text)
                        }
                        None => {
                            format!("{}={}\n", v.key, s.string)
                        }
                    },
                    BashVal::ValArray(a) => match a.len() {
                        1 => {
                            format!("{}=({})\n", v.key, a[0].string)
                        }
                        _ => {
                            let mut elem_text = String::new();
                            for i2 in a {
                                elem_text = format!("{}\n    {}", elem_text, i2.string);
                                if let Some(c) = &i2.comment {
                                    elem_text = format!("{} #{}", elem_text, c.text);
                                }
                            }
                            format!("{}=({}\n)\n", v.key, elem_text)
                        }
                    },
                },
                BashType::Function(f) => {
                    format!("{}({}) {{{}}}\n", f.name, f.args.join(" "), f.content)
                }
            };
            self.file.write(text.as_bytes());
        }
        Ok(())
    }
}
