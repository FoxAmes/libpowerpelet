# libpowerpelet

A `PKGBUILD` parsing library, written in Rust

## **Summary**

libpowerpelet arose from my need for a robust PKBGUILD parser to serve as the basis for a cross-compile helper for building archlinux packages. It is based on [nom](https://github.com/Geal/nom), which is used to implement a rudimentary bash parser which in turn populates the `PkgBuild` structure. This structure is an abstraction of well-defined `PKGBUILD` properties, specified in [the PKGBUILD manpage](https://man.archlinux.org/man/PKGBUILD.5), and can be updated and saved, propagating the changes to the underlying file.

## Notes

libpowerpelet is not currently stable or well-tested, so it should be used with caution. Issues and PRs are welcome!
